SKT Coding Challenge: Front End component of Solution by Adrian
===============================================================

This is the front end component of [my](https://www.linkedin.com/in/adrianyago/) of solution for SKTs Coding Challenge. This uses [Angular 6](https://blog.angular.io/version-6-of-angular-now-available-cc56b0efa7a4) with Server Side rendering via [Angular Universal](https://universal.angular.io/), to consume the RESTful services produced by the [backend component](https://bitbucket.org/AdrianY/skt-cc/). The UI components produced by this application are driven by the Hypermedia controls by the said services.

## Initialize the project

Run `npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
Files will be rendered by the browser.

## Build the app

Run `npm run build:ssr` to build the project. The build artifacts will be stored in the `dist/`  directories for
browser and server-side artifacts of the app.

## Run with angular universal support

Execute `npm run serve:ssr`. Navigate to `http://localhost:4000/`. Files will be rendered by an express engine instance (server side) when view resource is used via Chrome, which mimics web crawler action.


