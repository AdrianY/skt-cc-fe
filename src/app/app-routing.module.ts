import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
  {
    path: '',
    loadChildren: 'app/menu-one/menu-one.module#MenuOneModule'
  },
  {
    path: 'menu-one',
    loadChildren: 'app/menu-one/menu-one.module#MenuOneModule'
  },
  {
    path: 'menu-two',
    loadChildren: 'app/menu-two/menu-two.module#MenuTwoModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
