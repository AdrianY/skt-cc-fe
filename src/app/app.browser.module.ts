import {AppModule} from './app.module';
import {BrowserTransferStateModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';

@NgModule({
    declarations: [],
    imports: [
        BrowserTransferStateModule,
        AppModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppBrowserModule {}
