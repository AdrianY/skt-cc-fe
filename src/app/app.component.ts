import { environment } from '../environments/environment';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { isPlatformServer } from '@angular/common';
import { SeoService } from './core/services/seo.service';
import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';

const RESULT_KEY = makeStateKey<string>('result');
const BROWSER_RENDER_RESULT = 'I\'m created in the browser!';
const SERVER_RENDER_RESULT = 'I\'m created on the server!';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Static Site';
  private isServer: boolean;
  public renderResult: string;
  public prodEnabled;

  constructor(
    private _seo: SeoService,
    private tstate: TransferState,
    @Inject(PLATFORM_ID) platformId
  ) {
    this.prodEnabled = environment.production;
    this.isServer = isPlatformServer(platformId);
  }

  ngOnInit() {
      this._seo.setHeaders({
        siteTitle: this.title,
        description: 'A static site'
      });

      if (this.isServer) {
          this.tstate.set(RESULT_KEY, SERVER_RENDER_RESULT);
      }

      this.renderResult = this.tstate.get(RESULT_KEY, BROWSER_RENDER_RESULT);
  }
}
