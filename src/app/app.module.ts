import {AppRoutingModule} from './app-routing.module';
import {CoreModule} from './core/core.module';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'skt-cc-fe'}),
    AppRoutingModule,
    CoreModule
  ],
  providers: []
})
export class AppModule { }
