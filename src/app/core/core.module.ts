import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeoService } from './services/seo.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [SeoService]
})
export class CoreModule { }
