import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Injectable()
export class SeoService {

  private siteTitle = 'Ng Universal Testing';

  constructor(private _title: Title, private _meta: Meta) {}

  public setHeaders(data: SeoData) {
    this._title.setTitle([data.siteTitle, this.siteTitle].join(' | '));
      this._meta.updateTag({
        content: data.description,
        name: 'description'
      },
    );
  }
}

export interface SeoData {
  siteTitle?: any;
  description?: any;
}
