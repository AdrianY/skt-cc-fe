import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuOneComponent } from './components/menu-one/menu-one.component';

const routes: Routes = [
  {
    path: '',
    component: MenuOneComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuOneRoutingModule { }
