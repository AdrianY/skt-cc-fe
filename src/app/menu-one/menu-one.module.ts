import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuOneRoutingModule } from './menu-one-routing.module';
import { MenuOneComponent } from './components/menu-one/menu-one.component';

@NgModule({
  imports: [
    CommonModule,
    MenuOneRoutingModule
  ],
  declarations: [MenuOneComponent]
})
export class MenuOneModule { }
