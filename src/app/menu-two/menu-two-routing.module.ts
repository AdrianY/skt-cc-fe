import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuTwoComponent } from './components/menu-two/menu-two.component';

const routes: Routes = [
  {
    path: '',
    component: MenuTwoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuTwoRoutingModule { }
