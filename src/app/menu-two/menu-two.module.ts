import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuTwoRoutingModule } from './menu-two-routing.module';
import { MenuTwoComponent } from './components/menu-two/menu-two.component';

@NgModule({
  imports: [
    CommonModule,
    MenuTwoRoutingModule
  ],
  declarations: [MenuTwoComponent]
})
export class MenuTwoModule { }
